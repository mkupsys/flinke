import { applyMiddleware, compose, createStore} from 'redux';
import Reducers from '../reducers/index';
// import {combineReducers} from 'redux';

import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const middlewares = [thunk];

export default function configureStore(initialState) {
    const store = createStore(Reducers, initialState,
        composeEnhancers(applyMiddleware(...middlewares)));



    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers/index', () => {
            const nextRootReducer = require('../reducers/index');
            store.replaceReducer(nextRootReducer);
        });
    }
    return store;
}

