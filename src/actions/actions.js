import axios from 'axios';
import * as actionTypes from '../Constants/actionTypes/action';
import Notifications from 'react-notification-system-redux';

const userGuid = '62AEB29B-0118-48CA-B82B-D93CBA1522AC';
const notificationOpts = {
  title: 'Error',
  message: 'Something went wrong, please try again!',
  position: 'tc',
  level: 'error',
  autoDismiss: 4,
  dismissible:'click',
  action:null
};

export function getPortData(){
  return async function (dispatch) {
    dispatch({
      type:actionTypes.DATA_POST_LOADING
    })
    try {
      const GetInitData = await axios.post('http://demos.dev.flinkefolk.lt/Home/GetInitData', {
       userGuid : userGuid,
     })     
     dispatch({
      type: actionTypes.DATA_POST_SUCCESS,
      data: GetInitData.data
    });
    }       
     catch(error) {          
      return dispatch(
        getPortData(),{
        type: actionTypes.DATA_POST_FAIL,     
      })
    }
   };
  }

export function createItem(weight, volume)  {
  return async function (dispatch) {
    dispatch({
      type:actionTypes.CREATE_DATA_POST_LOADING
    })
    try {
      await axios.post('http://demos.dev.flinkefolk.lt/Home/CreateCargo', {
        userGuid : userGuid,
        weight: weight,
        volume: volume,
      })      
     dispatch(getPortData(),{
      type: actionTypes.CREATE_DATA_POST_SUCCESS,      
      });
    }
    catch(error) {         
     return (dispatch(Notifications.error(notificationOpts), dispatch(
      getPortData(),      
      {
      type: actionTypes.CREATE_DATA_POST_FAIL,
     }),
    ))
   }
  }
}

export function editItem(cargoId, weight, volume)  {
  return async function (dispatch) {
    dispatch({
      type:actionTypes.EDIT_DATA_POST_LOADING
    })
    try {
      await axios.post('http://demos.dev.flinkefolk.lt/Home/EditCargo', {
        userGuid : userGuid,
        cargoId: cargoId,
        weight: weight,
        volume: volume,
      })     
     dispatch(getPortData());
    }
    catch(error) {
     return (dispatch(Notifications.error(notificationOpts),
     dispatch(getPortData(),
      {
       type: actionTypes.EDIT_DATA_POST_FAIL,       
     })))
   }
  }
}


export function addToShip(cargoId, shipId) {
  return async function (dispatch) {
    dispatch({
      type:actionTypes.ADD_TO_SHIP_LOADING
    })
    try {
      await axios.post('http://demos.dev.flinkefolk.lt/Home/AddToShip', {
        userGuid : userGuid,
        cargoId: cargoId,
        shipId: shipId,        
      })      
     dispatch(getPortData());
    }
    catch(error) {
     return (dispatch(Notifications.error(notificationOpts),
      {
       type: actionTypes.ADD_TO_SHIP_FAIL,
     }),dispatch(getPortData()))
   }
  }
 };

export function addToDock(cargoId) {
  return async function (dispatch) {
    dispatch({
      type:actionTypes.ADD_TO_DOCK_LOADING
    })
    try {
      await axios.post('http://demos.dev.flinkefolk.lt/Home/AddToDock', {
        userGuid : userGuid,
        cargoId: cargoId,               
      })      
     dispatch(getPortData());
    }
    catch(error) {
     return (dispatch(Notifications.error(notificationOpts),
      {
       type: actionTypes.ADD_TO_DOCK_FAIL,       
     }),
     dispatch(getPortData()))
   }
  }
 };

