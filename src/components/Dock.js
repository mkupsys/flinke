import React, { Component } from 'react';
import { DropTarget } from "react-dnd";
import Source from './Source';

class Dock extends Component {
  toDock = (item) => {    
    this.props.addToDock(item.cargoId)
  }
    render(){
        const {connectDropTarget} = this.props;
          const mapedCargo = this.props.data.slice(0, 200).map((cargo) => (          
            <Source key={cargo.id}
            data={cargo} 
            steitas={this.props.steitas}
            handleWeight={this.props.handleWeight}
            handleVolume={this.props.handleVolume}
            blurHandle={this.props.blurHandle}
            changeHandler={this.props.changeHandler}/>           
        )) 
        return connectDropTarget(
            <div>
                {mapedCargo}
            </div>
        )
    }
}

const spec = {
    drop(props, monitor, component) {
      const item = monitor.getItem();      
      component.toDock(item);      
    },
    hover(props, monitor, component){      
    },
    canDrop(props, monitor) {
      if (props.data.length > 200) {
        return false
      }else {
        return true
      }      
    }
  };
  function collect(connect, monitor) {
    return {
      connectDropTarget: connect.dropTarget(),
      isOver: monitor.isOver(),     
      canDrop: monitor.canDrop()
    };
  }
  
  export default DropTarget("dock", spec, collect)(Dock);