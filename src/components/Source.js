/* eslint-disable eqeqeq */
import React, { Component } from "react";
import { DragSource } from "react-dnd";


class Source extends Component {
  render() {    
    const {  connectDragSource, isDragging } = this.props;
    const opacity = isDragging ? 0 : 1
    return connectDragSource(  <div key={this.props.data.id}>      
      <div className="cargo" style={{opacity}}>
        <div className="value">
          <div className="name" key={this.props.data.id} data-tag={this.props.data.id} >Cargo Id: {this.props.data.id}</div>               
            <div onDoubleClick={(event) => {this.props.handleWeight(event)}} >         
              <div className="name" 
                style={this.props.steitas.weight == this.props.data.id ? {display: 'none'} : {color: 'red'}} 
                name={this.props.data.id} data-tag={this.props.data.weight} key={this.props.data.weight}>
                Cargo weight: {this.props.data.weight}</div>
              <div style={this.props.steitas.weight == this.props.data.id ? {color: 'red'} : {display: 'none'}} >Cargo weight:
                <input id="weight" data-tag={this.props.data.id} name={this.props.data.volume}  placeholder={this.props.data.weight} type="text" value={this.props.steitas.test} 
                  onChange={(event) => this.props.changeHandler(event)} 
                  onBlur={(event) => this.props.blurHandle(event)} />
              </div>
            </div>  
            <div onDoubleClick={(event) => {this.props.handleVolume(event)}} >         
              <div className="name" 
                style={this.props.steitas.volume == this.props.data.id ? {display: 'none'} : {color: 'red'}} 
                name={this.props.data.id} data-tag={this.props.data.volume} key={this.props.data.volume}>
                Cargo volume: {this.props.data.volume}</div>
              <div style={this.props.steitas.volume == this.props.data.id ? {color: 'red'} : {display: 'none'}} >Cargo volume:
                <input id="volume" data-tag={this.props.data.id} name={this.props.data.weight} placeholder={this.props.data.volume}  type="text" value={this.props.steitas.test} 
                  onChange={(event) => this.props.changeHandler(event)}
                  onBlur={(event) => this.props.blurHandle(event)} />
              </div>
            </div>     
          </div>
        </div>
      </div>)
  }
}

const spec = {  
  beginDrag(props) {    
    return {
        cargoId: props.data.id,
        cargoWeight: props.data.weight,
        cargoVolume: props.data.volume,
    };    
  },  
canDrag(props, monitor) {
    return true;
  },
endDrag(props, monitor) {    
  }
}

const collect = (connect, monitor) => {
  return {
    connectDragSource: connect.dragSource(spec),
    isDragging: monitor.isDragging()
  };
}

export default DragSource("cargo", spec, collect)(Source);