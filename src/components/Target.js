/* eslint-disable eqeqeq */
import React, { Component } from "react";
import { DropTarget } from "react-dnd";
import InShip from './InShip.js'

class Target extends Component {
  constructor (props) {
    super(props)
    this.state = {    
    calcVolume: 0,
    calcWeight: 0,
    percentW: 0,
    percentV: 0,
  }
}
  getMeSomeStuff = (item) => {  
    this.props.addToShip(item.cargoId, this.props.data.id)
  }
  calculate = () => {    
    if(this.props.data.cargoItems != 0){
      const calcWeight = this.props.data.cargoItems.map(t => t.weight).reduce((acc, item) => acc+= item, 0);
      const calcVolume = this.props.data.cargoItems.map(t => t.volume).reduce((acc, item) => acc+= item, 0);     
      const percentW = ((calcWeight/this.props.data.maxWeight) * 100).toFixed(2);  
      const percentV = ((calcVolume/this.props.data.maxVolume) * 100).toFixed(2);   
      this.setState({
        calcVolume: calcVolume,
        calcWeight: calcWeight,
        percentW: percentW,
        percentV: percentV
      })
    }
  }
 
  componentDidMount(){
    this.calculate();
  }
  render() {  
    const {  connectDropTarget, isOver, canDrop } = this.props;    
    let background = "";
    if (isOver && canDrop) {
      background = "green";
    } else if (!isOver && canDrop) {
      background = "yellow";
    } else if (isOver && !canDrop) {
      background = "red";
    }    
    const cargoInShip = this.props.data.cargoItems.map(cargo => (
      <InShip key={cargo.id} data={cargo}/>
    ))    
    return connectDropTarget(
      <div key={this.props.data.id}>           
      <div className="ships" style={{background}}>
        <div className="value">
          <div  className="name" key={this.props.data.name}>Name: {this.props.data.name}</div>
          <div className="name" key={this.props.data.id}>Id: {this.props.data.id}</div>            
        </div>
        <div className="value">
          <div className="name" key={this.props.data.maxWeight}>
            Max Weight: {this.state.calcWeight} / {this.props.data.maxWeight}</div>
            <div className="name">Weight percentage: {this.state.percentW} %</div>
          <div className="name" key={this.props.data.maxVolume}>Max Volume: {this.state.calcVolume} / {this.props.data.maxVolume}</div>  
          <div className="name">Volume percentage: {this.state.percentV} %</div>          
        </div>
        <div> Cargo items: {cargoInShip}</div>        
      </div>            
    </div>
    );
  }
}

const spec = {
  drop(props, monitor, component) {
    const item = monitor.getItem();    
    component.getMeSomeStuff(item);    
  },
  hover(props, monitor, component){    
  },
  canDrop(props, monitor) {
    let test; 
    const dragData = monitor.getItem();
    const volume = props.data.cargoItems.map(t => t.volume).reduce((acc, item) => acc+= item, 0);
    const weight = props.data.cargoItems.map(t => t.weight).reduce((acc, item) => acc+= item, 0);
    if((volume + dragData.cargoVolume) > props.data.maxVolume || (weight + dragData.cargoWeight) > props.data.maxWeight){
      test = false      
    } else {
      test = true      
    }
    return test
  }
};
function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),    
    canDrop: monitor.canDrop()
  };
}

export default DropTarget("cargo", spec, collect)(Target);