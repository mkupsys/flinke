/* eslint-disable eqeqeq */
import React, { Component } from "react";
import { DragSource } from "react-dnd";

class InShip extends Component {
    render() {        
        const {  connectDragSource, isDragging } = this.props;
        const opacity = isDragging ? 0 : 1;
       
        return connectDragSource(
          <div className="ships" style={{opacity}}>  
                  <div>Id: {this.props.data.id}</div>
                  <div>Weight: {this.props.data.weight}</div>
                  <div>Volume: {this.props.data.volume}</div>   
                </div>            
        )
    }
}
const spec = {    
    beginDrag(props) {      
      return {
          cargoId: props.data.id,
          cargoWeight: props.data.weight,
          cargoVolume: props.data.volume,
      };      
    },   
  canDrag(props, monitor) {
      return true;
    },
  endDrag(props, monitor) {   
    }
  }  
  const collect = (connect, monitor) => {
    return {
      connectDragSource: connect.dragSource(spec),
      isDragging: monitor.isDragging()
    };
  }
  
  export default DragSource("dock", spec, collect)(InShip);