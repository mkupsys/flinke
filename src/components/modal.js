import React, { Component } from 'react';
import  "../App.css";

class Create extends Component {
    render() {
        return (
            <div className="create">
                <form>
                    <input placeholder="Cargo weight" type="text"/>
                    <input placeholder="Cargo Volume" type="text"/>
                    <button onClick={this.cancelModal}>Cancel</button>
                    <button onClick={this.saveCreate}>Save</button>
                </form>
            </div>
        )
    }
}

export default Create