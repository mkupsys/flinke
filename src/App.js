/* eslint-disable eqeqeq */
import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Notifications from 'react-notification-system-redux';
import Target from './components/Target.js';
import Dock from './components/Dock.js';
import * as actions from './actions/actions';


let modal;

class App extends Component { 
  constructor (props) {
    super(props)
    this.state = {
      test: "",
      showModal: false,
      newWeight: "",
      newVolume: "",
      weight: "",
      volume: "",
      canDrop: true,
    }
  }

componentDidMount(){  
  this.props.getPortData()
}

handleWeight = (event) => {
const saveName = event.target.getAttribute('name');
  this.setState({ 
    weight: saveName,    
  });
}

handleVolume = (event) => {
  const saveName = event.target.getAttribute('name');  
    this.setState({ 
      volume: saveName,    
    });
  }
  
blurHandle = (event) => {
  const id = event.target.getAttribute('id');
  const saveId = event.target.getAttribute('data-tag');
  if (id == "weight") {
    const saveVolume = event.target.name;
    const saveValue = event.target.value;
    this.props.editItem(saveId, saveValue, saveVolume);
    this.setState({
      weight: "",    
    })
  }else if (id == "volume") {
    const saveWeight = event.target.name;
    const saveValue = event.target.value;
    this.props.editItem(saveId, saveWeight, saveValue);
    this.setState({      
      volume: "",      
    })
  }else{
    console.log("Hello world")
  }  
    this.setState({     
      test: "",
    })
  }
changeHandler = (event) => {
  const updateData = event.target.value;
  this.setState({
    test: updateData,
  })  
}
cancelModal = () => {
  this.setState({
    showModal: false,
  })
}
saveCreate = (event) => {
  event.preventDefault();
  const weight = this.state.newWeight;
  const volume = this.state.newVolume;  
  this.props.createItem(weight, volume);
  this.setState({
    showModal: false,
    newWeight: "",
    newVolume: "",
  })
}
openModal = () => {
  this.setState({
    showModal: true,
  })
}
getWeight = (event) => {
  this.setState({
    newWeight: event.target.value,
  })  
}
getVolume = (event) => {
  this.setState({
    newVolume: event.target.value,
  })  
}

render() {
 
  const {notifications} = this.props;
  const style = {
    NotificationItem: { 
      DefaultStyle: { 
        margin: '10px 5px 2px 1px'
      },
      success: { 
        color: 'red'
      }
    }
  };
  let dataReceived;  
  if(this.state.showModal){    
     modal = <div className="create">
                <div className="create-form">
                <div className="inputs">
                  <div>
                    <input placeholder="Cargo weight" type="text" name={"weight"}
                     onChange={this.getWeight} value={this.state.newWeight}/>
                  </div>
                  <div>                    
                    <input placeholder="Cargo Volume" type="text" name={"volume"}
                     onChange={this.getVolume} value={this.state.newVolume}/>
                  </div>
                </div>
                <button onClick={this.cancelModal} className="cancelButton">Cancel</button>
                <button onClick={this.saveCreate} className="myButton">Save</button>
                </div>                
            </div>       
  }else {
    modal = <div></div>
  }
  if(this.props.Item.loading) {
    dataReceived = (
    <div className="loadme">
      <div className="load">Loading</div>
      <div className="loader">
      <div className="face">
        <div className="circle"></div>
      </div>      
      <div className="face">
        <div className="circle"></div>
      </div>
      </div>
    </div>);
  }else {
    const shiped = this.props.Item.data.ships;
    const shipCounter = shiped.length < 5 ? "Ships: "+shiped.length+"/5" : "Ship limit is reached, therefore only first 5 ships will be shown!";    
    const cargo = this.props.Item.data.dock.cargoItems;
    const cargoCounter = cargo.length < 200 ? "Cargo: "+cargo.length+"/200" : "Cargo limit is reached, therefore only first 200 cargo will be shown!";
    
    const mapedShips = shiped.slice(0, 5).map(vasia => (      
       <Target 
       key={vasia.id} 
       data={vasia} 
       steitas={this.state}
       addToShip={this.props.addToShip}/>
        ))          
    dataReceived = 
    <div className="all">
      <div className="testing"> 
      <div>{shipCounter}</div>     
      {mapedShips}
      </div>
       <div className="testing">
         <button onClick={this.openModal} className="createButton">Create cargo</button>
         <div className="cargoCounter">{cargoCounter}</div>
         <Dock key={cargo.id} data={cargo}       
         steitas={this.state}
         handleWeight={this.handleWeight}
         handleVolume={this.handleVolume}
         blurHandle={this.blurHandle}
         changeHandler={this.changeHandler}
         addToDock={this.props.addToDock}
        >           
        </Dock>
      </div> 
    </div>
  };
  
 return (
      
       <div className="App">
        {modal}
        {dataReceived}
        <Notifications
        notifications={notifications}
        style={style}
      />        
       </div>
       
     );
  }
}

App.propTypes = {
  getWeight: PropTypes.func,
  getVolume: PropTypes.func,
  cancelModal: PropTypes.func,
  saveCreate: PropTypes.func,
  openModal: PropTypes.func,
  handleVolume: PropTypes.func,
  handleWeight: PropTypes.func,
  blurHandle: PropTypes.func,
  changeHandler: PropTypes.func,
  newVolume: PropTypes.number,
  newWeight: PropTypes.number,
  weight: PropTypes.number,
  volume: PropTypes.number,
  saveValue: PropTypes.number,
  saveWeight: PropTypes.number,
  saveVolume: PropTypes.number,
  saveId: PropTypes.number,  
  mapedCargo: PropTypes.node,
  mapedShips: PropTypes.node,
  modal:PropTypes.node,
  dataReceived: PropTypes.node,
  showModal: PropTypes.bool,
  shiped: PropTypes.object,
  cargo: PropTypes.object,
  id: PropTypes.string,
  test: PropTypes.string,
  notifications: PropTypes.array,
  saveName: PropTypes.number, 
}
const mapStateToProps = state => {
  return {
    Item: state.Item,
    notifications: state.notifications
  }
}
const mapDispatchToProps = dispatch => {
  return {
    getPortData: () => dispatch(actions.getPortData()),
    editItem: (cargoId, weight, volume) => dispatch(actions.editItem(cargoId, weight, volume)),
    createItem: (weight, volume) => dispatch(actions.createItem(weight, volume)),
    addToShip: (cargoId, shipId) => dispatch(actions.addToShip(cargoId, shipId)),
    addToDock: (cargoId) => dispatch(actions.addToDock(cargoId))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);

