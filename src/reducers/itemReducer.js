import * as actionTypes from '../Constants/actionTypes/action';

const initialState = {
    data: null,
    error: {},
    loading: true
}

const itemReducer = (state = initialState, action) =>   {
    // eslint-disable-next-line default-case
    switch(action.type) {
        case actionTypes.DATA_POST_SUCCESS: 
        return {
            ...state,
            data: action.data,
            loading: false
        };
        case actionTypes.DATA_POST_LOADING:
        return {
            ...state,           
            loading: true
        };
        case actionTypes.DATA_POST_FAIL:
        return {
            ...state,
            loading: false, 
            error: 'Something went wrong, please try again!'
        };
        case actionTypes.EDIT_DATA_POST_LOADING:
        return {
            ...state,
            loading: true
        };
        case actionTypes.EDIT_DATA_POST_SUCCESS:
        return {
            ...state,
            data: action.data,
            loading: false
        };
        case actionTypes.EDIT_DATA_POST_FAIL:
        return {
            ...state,
            loading: false,
            error: 'Something went wrong, please try again!'
        };
        case actionTypes.CREATE_DATA_POST_LOADING:
        return {
            ...state, 
            loading: true
        };
        case actionTypes.CREATE_DATA_POST_SUCCESS:
        return {
            ...state,
            data: action.data,
            loading: false
        };
        case actionTypes.CREATE_DATA_POST_FAIL:
        return {
            ...state,
            loading: false,
            error: 'Something went wrong, please try again!'
        };
        case actionTypes.ADD_TO_SHIP_LOADING:
            return {
                ...state,
                loading: true
            };
        case actionTypes.ADD_TO_SHIP_SUCCESSS:
        return {
            ...state,
            data: action.data,
            loading: false
        };
        case actionTypes.ADD_TO_SHIP_FAIL:
        return {
            ...state,
            loading: false,
            error: 'Something went wrong, please try again!'
        };
        case actionTypes.ADD_TO_DOCK_LOADING:
        return {
            ...state,
            loading: true
        };
        case actionTypes.ADD_TO_DOCK_SUCCESS:
        return {
            ...state,
            data: action.data,
            loading: false
        };
        case actionTypes.ADD_TO_DOCK_FAIL:
        return {
            ...state,
            loading: false,
            error: 'Something went wrong, please try again!'
        };
        default: return state;
             
    }
    
}

export default itemReducer;