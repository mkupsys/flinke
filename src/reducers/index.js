import {combineReducers} from 'redux';
import {reducer as notifications} from 'react-notification-system-redux';
import ItemReducer from './itemReducer';
const reducers = combineReducers({
    Item:ItemReducer,
    notifications,
});

export default reducers;
